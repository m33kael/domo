﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sunlight : MonoBehaviour
{

    public GameObject controller;
    private Constant constant;
    public string texte = "Luminosité";
    // Use this for initialization
    void Start()
    {
        constant = controller.GetComponent<Constant>();

    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.GetComponent<Text>().text = texte + constant.lighting + " %";
    }
}
