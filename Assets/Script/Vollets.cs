﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vollets : MonoBehaviour {

    private Vector3 height = new Vector3(0.0f, 1.4f, 0.0f);
    private bool down = true;
    private Vector3 end;
    
    private float speed = 0.3f;
    private Vector3 start;

    // Use this for initialization
    void Start () {
        start= this.gameObject.transform.position;
        end = start;
        end.y = end.y + height.y;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("v"))
        {
            if (down)
            {
                
                down = false;
            }
            else
            {
         
                down = true;
            }
        }
        if (down && this.gameObject.transform.position.y >  start.y )
        {
            this.gameObject.transform.Translate(0.0f, -speed * Time.deltaTime, 0.0f );
        }
        if (!down && this.gameObject.transform.position.y <  end.y)
        {
            this.gameObject.transform.Translate(0.0f, speed * Time.deltaTime, 0.0f);
        }
    }

    

    
}
