﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant : MonoBehaviour {

    public int lighting = 100;
    public int decrease = 5;
    public int hour = 8;
    
    private float maxExp = 1.45f;
    private float minExp = 0.0f;
    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown("h"))
        {
            hour--;
        }else if (Input.GetKeyDown("y"))
        {
            hour++;
        }

        if (hour  > 23)
        {
            hour = 0;
        }
        if( hour < 0)
        {
            hour = 23;
        }

       

        if (Input.GetKeyDown("l")){
            lighting -= decrease;
        }else if(Input.GetKeyDown("o")) {
            lighting += decrease;
        }

        

        RenderSettings.skybox.SetFloat("_Exposure", ((maxExp - minExp) * lighting /100 )+minExp);
       
        
	}
}
