﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HourDisplay : MonoBehaviour {

    public GameObject controller;
    private Constant constant;
    public string texte = "heure";
	// Use this for initialization
	void Start () {
        constant = controller.GetComponent<Constant>();

    }
	
	// Update is called once per frame
	void Update () {
        this.gameObject.GetComponent<Text>().text = constant.hour + " " + texte;
	}
}
