﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light2 : MonoBehaviour {

    private GameObject gameController;
    private Constant constant;
    public int trigger = 40;
    public float lightForce = 100;
    private float standardLight = 100;
	// Use this for initialization
	void Start () {
        gameController = GameObject.Find("Gamecontroller").gameObject;
        constant = gameController.GetComponent<Constant>();
    }
	
	// Update is called once per frame
	void Update () {
        if ( constant.lighting < trigger)
        {
            
            GetComponent<Light>().intensity = lightForce / standardLight;
        }
        else
        {
            GetComponent<Light>().intensity = 0;
        }
                
        
	}
}
